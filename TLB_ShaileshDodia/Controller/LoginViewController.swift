//
//  ViewController.swift
//  TLB_ShaileshDodia
//
//  Created by Shailesh Dodia on 31/12/18.
//  Copyright © 2018 Shailesh Dodia. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, DataModelDelegate
{
    @IBOutlet weak var txtUserName: SDFloatingLabelTF!
    @IBOutlet weak var txtPassword: SDFloatingLabelTF!
    @IBOutlet weak var btnProceed: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.txtPassword.isSecureTextEntry = true
        
//        self.txtUserName.text = "zicom1"
//        self.txtPassword.text = "aaaaaa"
    }
    
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    @IBAction func btnProceedPressed(_ sender: Any)
    {
        if (self.txtUserName.text?.isEmpty)!
        {
            Utilities.showAlert(WithTitle: "Error", message: Constants.validationMessages.emptyUserName)
            return
        }
        else if (self.txtPassword.text?.isEmpty)!
        {
            Utilities.showAlert(WithTitle: "Error", message: Constants.validationMessages.emptyPassword)
            return
        }
        
        self.requestToLogin()
    }
    
    func requestToLogin()
    {
        let request = LoginRequest()
        request.username = self.txtUserName.text
        request.password = self.txtPassword.text
        request.latitude = "12121212"
        request.longitude = "1212121212"
        request.type = "enterprise"  //mycs,enterprise,channel_partner,employee
        request.device = "iOS" // (json object: mobile, os, imei,type)
        
        Utilities.CustomActivityIndicator(startAnimate: true)
        LoginModel.delegateLoginModel = self
        LoginModel.requestToLogin(loginRequest: request)
    }
    
    func successResponse(response: Any?)
    {
        Utilities.CustomActivityIndicator(startAnimate: false)
        
        if let loginResponse = response as? LoginResponse
        {
            let singleton = Singleton.sharedManager
            singleton.strToken = loginResponse.data.token
            
            let listComplaintVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListComplaintsVC") as! ListComplaintsVC
            listComplaintVC.loginResponse = loginResponse
            self.navigationController?.pushViewController(listComplaintVC, animated: true)
        }
    }
    
    
    func failResponse(response: Any?)
    {
        Utilities.CustomActivityIndicator(startAnimate: false)
        
        if let message = response as? String
        {
            self.txtPassword.resetTextField()
            Utilities.showAlert(WithTitle: "Error", message: message)
        }
        else
        {
            self.txtPassword.resetTextField()
            Utilities.showAlert(WithTitle: "Error", message: Constants.validationMessages.somethingwrong)
        }
    }
}

