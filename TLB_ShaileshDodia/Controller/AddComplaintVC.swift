//
//  AddComplaintVC.swift
//  TLB_ShaileshDodia
//
//  Created by Shailesh Dodia on 01/01/19.
//  Copyright © 2019 Shailesh Dodia. All rights reserved.
//

import UIKit

class AddComplaintVC: UIViewController, DataModelDelegate, UITextViewDelegate
{
    @IBOutlet weak var txtTitle: SDFloatingLabelTF!
    @IBOutlet weak var txtMobileNumber: SDFloatingLabelTF!
    @IBOutlet weak var txtDescription: UITextView!

    var placeholderLabel : UILabel!
    var loginResponse = LoginResponse()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.txtDescription.layer.borderWidth = 1
        
        self.title = "Raise a new Complaint"
        
        self.setTextViewPlaceholder()
    }
    
    func setTextViewPlaceholder()
    {
        self.txtDescription.layer.borderColor = UIColor.lightGray.cgColor
        self.txtDescription.layer.cornerRadius = 5
        
        self.txtDescription.delegate = self
        placeholderLabel = UILabel()
        placeholderLabel.text = "Enter Description"
        placeholderLabel.font = UIFont.italicSystemFont(ofSize: (self.txtDescription.font?.pointSize)!)
        placeholderLabel.sizeToFit()
        self.txtDescription.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPoint(x: 5, y: (self.txtDescription.font?.pointSize)! / 2)
        placeholderLabel.textColor = UIColor.lightGray
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    @IBAction func btnSubmitPressed(_ sender: Any)
    {
        if (self.txtTitle.text?.isEmpty)!
        {
            Utilities.showAlert(WithTitle: "Error", message: "Please enter complaint title")
            return
        }
        else if (self.txtMobileNumber.text?.isEmpty)!
        {
            Utilities.showAlert(WithTitle: "Error", message: "Please enter mobile number")
            return
        }
        else if (self.txtDescription.text?.isEmpty)!
        {
            Utilities.showAlert(WithTitle: "Error", message: "Please enter complaint description")
            return
        }

        self.requestToRaiseComplaint()
    }
    
    func requestToRaiseComplaint()
    {
        let addComplaintRequest = AddComplaintRequest()
        addComplaintRequest.complainant_name = self.loginResponse.data.name
        addComplaintRequest.asset = self.loginResponse.data.asset
        addComplaintRequest.registered_name = self.loginResponse.data.name
        addComplaintRequest.complainant_number = self.txtMobileNumber.text
        addComplaintRequest.registered_number = self.txtMobileNumber.text
        addComplaintRequest.title = self.txtTitle.text
        addComplaintRequest.description = self.txtDescription.text
        addComplaintRequest.latitude = "1212121"
        addComplaintRequest.longitude = "121212"

        Utilities.CustomActivityIndicator(startAnimate: true)
        AddComplaintModel.delegateAddComplaintModel = self
        AddComplaintModel.requestToAddComplaint(addComplaintRequest: addComplaintRequest)
    }
    
    
    func successResponse(response: Any?)
    {
        Utilities.CustomActivityIndicator(startAnimate: false)

        if let addComplaintResponse = response as? AddComplaintResponse
        {
            Utilities.showAlert(WithTitle: "", message: addComplaintResponse.message)
            
            let uialertController = UIAlertController.init(title: "", message: addComplaintResponse.message, preferredStyle: .alert)

            let okActionButton = UIAlertAction(title: "OK", style: .cancel) { _ in
                self.navigationController?.popViewController(animated: true)
            }
            uialertController.addAction(okActionButton)

            UIApplication.topViewController()?.present(uialertController, animated: true, completion: nil)
        }
    }
    
    
    func failResponse(response: Any?)
    {
        Utilities.CustomActivityIndicator(startAnimate: false)

        if response is String
        {
            let uialertController = UIAlertController.init(title: "Error", message: response as? String , preferredStyle: .alert)
            
            let okActionButton = UIAlertAction(title: "OK", style: .cancel) { _ in
                self.navigationController?.popViewController(animated: true)
            }
            uialertController.addAction(okActionButton)
            
            UIApplication.topViewController()?.present(uialertController, animated: true, completion: nil)
        }
    }
}
