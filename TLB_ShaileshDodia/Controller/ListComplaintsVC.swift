//
//  ComplaintListingVC.swift
//  TLB_ShaileshDodia
//
//  Created by Shailesh Dodia on 01/01/19.
//  Copyright © 2019 Shailesh Dodia. All rights reserved.
//

import UIKit

class ListComplaintsVC: UIViewController, DataModelDelegate
{
    @IBOutlet weak var tableViewList: UITableView!

    var loginResponse = LoginResponse()
    var arrComplaints = [ComplaintObj]()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.title = "Your Complaints"
        
        self.navigationItem.hidesBackButton = true
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(btnAddComplaintPressed))

        self.tableViewList.tableFooterView = UIView()
    }

    override func viewWillAppear(_ animated: Bool)
    {
        self.requestToListComplaints()
    }
    
    
    func requestToListComplaints()
    {
        let listComplaintRequest = ListComplaintsRequest()
        listComplaintRequest.asset = "Uno-24989"    //  self.loginResponse.data.asset
        listComplaintRequest.status = ""
        listComplaintRequest.ticket = ""
        listComplaintRequest.offset = 0
        
        Utilities.CustomActivityIndicator(startAnimate: true)
        ListComplaintsModel.delegateListComplaints = self
        ListComplaintsModel.requestToListComplaints(listComplaintsRequest: listComplaintRequest)
    }
    
    func successResponse(response: Any?)
    {
        Utilities.CustomActivityIndicator(startAnimate: false)
        
        if let successResponse = response as? ListComplaintResponse
        {
            self.arrComplaints = successResponse.data.complaints
            self.tableViewList.reloadData()
        }
    }
    
    
    func failResponse(response: Any?)
    {
        Utilities.CustomActivityIndicator(startAnimate: false)
    }
    
    @IBAction func btnAddComplaintPressed(_ sender: Any)
    {
        let addComplaintVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddComplaintVC") as! AddComplaintVC
        addComplaintVC.loginResponse = self.loginResponse
        self.navigationController?.pushViewController(addComplaintVC, animated: true)
    }
}


extension ListComplaintsVC: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrComplaints.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableViewList.dequeueReusableCell(withIdentifier: "kCellReusableID", for: indexPath)
        
        let complaint = arrComplaints[indexPath.row]
        
        cell.textLabel?.text = complaint.request_description
        
        cell.textLabel?.numberOfLines = 0
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.tableViewList.deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 44
    }
}
