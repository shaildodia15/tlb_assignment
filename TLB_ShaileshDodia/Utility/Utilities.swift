//
//  Utilities.swift
//  TLB_ShaileshDodia
//
//  Created by Shailesh Dodia on 31/12/18.
//  Copyright © 2018 Shailesh Dodia. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

class Utilities: NSObject
{
    //To show custom indicator
    static func CustomActivityIndicator(startAnimate:Bool? = true)
    {
        DispatchQueue.main.async {
            if startAnimate!
            {
                let loadingNotification = MBProgressHUD.showAdded(to: (UIApplication.topViewController()?.view)!, animated: true)
                
                loadingNotification.backgroundView.tintColor = UIColor.black
                loadingNotification.mode = MBProgressHUDMode.indeterminate
                loadingNotification.label.text = "Please Wait"
            }
            else
            {
                MBProgressHUD.hide(for:(UIApplication.topViewController()?.view)!, animated: true)
            }
        }
    }
    
    //To check internet connection
    static func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    
    //TO show alert
    static func showAlert(WithTitle title: String, message: String?)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil))
        
        let currentVC = UIApplication.topViewController()
        DispatchQueue.main.async {
            currentVC?.present(alert, animated: true, completion: nil)
        }
    }
    
    
    //To print logs in Debug mode only
    static func printLog(key: String, value: Any)
    {
        #if DEBUG
        print("\(key) = ")
        print("\(value)\n\n\n")
        #endif
    }
}
