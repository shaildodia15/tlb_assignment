//
//  SDFloatingLabel.swift
//  TLB_ShaileshDodia
//
//  Created by Shailesh Dodia on 31/12/18.
//  Copyright © 2018 Shailesh Dodia. All rights reserved.
//

import UIKit

enum AnimationType : Int {
    case animate_UP = 0
    case animate_DOWN = 1
}

protocol CustomTxtFldDelegate:UITextFieldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
}

class SDFloatingLabelTF: UITextField, UITextFieldDelegate
{
    var animated = false
    var orginalframe = CGRect()
    var lblFloating = UILabel()
    var txtLine = UIView()
    var customFieldDelegate: CustomTxtFldDelegate?
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    
    func commonInit()
    {
        animated = false
        lblFloating.isHidden = true
        
        self.autocorrectionType = .no
        self.delegate = self
        self.borderStyle = .none
        
        //        self.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        self.drawLine()
        self.setlblFloating()
    }
    
    
    func drawLine()
    {
        txtLine.frame = CGRect(x: 0, y: 34, width: self.frame.width, height: 1.3)
        self.txtLine.backgroundColor = UIColor.lightGray
        self.addSubview(txtLine)
    }
    
    func setlblFloating()
    {
        lblFloating = UILabel()
        lblFloating.isHidden = true
        
        lblFloating.text = self.placeholder
        lblFloating.backgroundColor = UIColor.clear
        lblFloating.textAlignment = self.textAlignment
        
        lblFloating.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        lblFloating.textColor = UIColor.darkGray
        
        lblFloating.font = self.font!.withSize(12)
        
        orginalframe = lblFloating.frame;
        
        self.addSubview(lblFloating)
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if (textField.textInputMode?.primaryLanguage == "emoji") || !((textField.textInputMode?.primaryLanguage) != nil)
        {
            return false
        }
        else if let result = customFieldDelegate?.textField(textField, shouldChangeCharactersIn: range, replacementString: string)
        {
            if self.updateTextForTextfield(string: string, range: range)
            {
                if result == true  && string != ""
                {
                    animateTextFieldwithString(string, textField, orginalframe, lbl: lblFloating, animationType: AnimationType.animate_UP)
                }
                return result
            }
            else
            {
                return self.updateTextForTextfield(string: string, range: range)
            }
        }
        else if self.updateTextForTextfield(string: string, range: range)
        {
            animateTextFieldwithString(string, textField, orginalframe, lbl: lblFloating, animationType: AnimationType.animate_UP)
        }
        return self.updateTextForTextfield(string: string, range: range)
    }
    
    
    func updateTextForTextfield(string:String,range: NSRange) -> Bool
    {
        let newLength = self.text!.count + string.count - range.length
        
        if newLength <= 0 && string.count <= 0
        {
            animated = false
            lblFloating.frame = orginalframe
            lblFloating.isHidden = true
        }
        return true
    }
    
    
    func animateTextFieldwithString(_ string: String,_ txtfield: UITextField,_ orgframe: CGRect, lbl: UILabel, animationType: AnimationType)
    {
        if self.text?.count == 0 && !(string == "\n")
        {
            self.lblFloating.isHidden = false
            
            UIView.animate(withDuration: 0.3, animations: {() -> Void in
                self.lblFloating.frame = CGRect(x: lbl.frame.origin.x, y: lbl.frame.origin.y-18, width: lbl.frame.size.width, height: self.lblFloating.frame.size.height)
            }) { _ in }
        }
        else if string.count == 0 && txtfield.text?.count == 1
        {
            resetTextField()
        }
    }
    
    //Reset textfield for password field
    func resetTextField()
    {
        self.text = ""
        animated = false
        lblFloating.frame = orginalframe
        lblFloating.isHidden = true
    }
}


extension CustomTxtFldDelegate
{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        return true
    }
}

