//
//  Constants.swift
//  TLB_ShaileshDodia
//
//  Created by Shailesh Dodia on 31/12/18.
//  Copyright © 2018 Shailesh Dodia. All rights reserved.
//

import UIKit

class Constants: NSObject
{
    override init()
    {
        super.init()
    }
    
    struct urls
    {
        static let urlLogin = "http://13.232.35.230/api/user/login"
        static let urlListComplaint = "http://13.232.35.230/api/complaint/list"
        static let urlRaiseComplain = "http://13.232.35.230/api/complaint/submit"
    }
    
    struct parameterLimit
    {
        static let limit = 10
        static let sessionTimeInterval          = 120.0 //In Seconds
    }
    
    struct validationMessages
    {
        static let error                    = "Error"
        static let unableToConnect          = "Unable to connect to the server"
        static let somethingwrong           = "Something went wrong. Please try again."
        static let noConnection             = "No Internet Connection"
        static let invalidURL               = "Invalid URL to Connect"
        static let emptyUserName            = "Please enter user name"
        static let emptyPassword            = "Please enter password"
    }
    
    struct colorCode
    {
        static let emptyTxtFieldBorder       = "#979797"
        static let filledTxtFieldBorder      = "#6746d1"
        static let placeHolderTextColor      = "#747d85"
        static let emptyVerticalLine         = "#c6c6c6"
        static let filledVerticalLine        = "#6746d1"
    }
}
