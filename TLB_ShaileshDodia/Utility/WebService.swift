//
//  WebUtilities.swift
//  TLB_ShaileshDodia
//
//  Created by Shailesh Dodia on 31/12/18.
//  Copyright © 2018 Shailesh Dodia. All rights reserved.
//

import Foundation
import Alamofire

public typealias Parameters = [String: Any]

class WebService: NSObject
{
    static func requestServiceWith(url: String, parameters: Parameters, completionHandler: @escaping (Any?, String?) -> Swift.Void)
    {

        Utilities.printLog(key: "URL", value: url)
        guard let encoddedURL = URL(string: url) else {
            Utilities.CustomActivityIndicator(startAnimate: false)
            Utilities.showAlert(WithTitle: Constants.validationMessages.error,
                                message: Constants.validationMessages.invalidURL)
            return
        }

        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = Constants.parameterLimit.sessionTimeInterval
        configuration.httpAdditionalHeaders = ["Content-Type":"application/x-www-form-urlencoded"]

        let singleton = Singleton.sharedManager
        if let tokan = singleton.strToken
        {
            configuration.httpAdditionalHeaders = ["token": tokan]
        }

        
        let manager = Alamofire.SessionManager(configuration: configuration)
        
        manager.request(encoddedURL, method: .post, parameters:parameters).validate().responseJSON { (response) -> Void in

            guard response.result.isSuccess else{
                if let message = response.result.error?.localizedDescription
                {
                    completionHandler(nil, message)
                }
                else
                {
                    completionHandler(nil, Constants.validationMessages.somethingwrong)
                }
                return
            }

            guard let jsonResponse = response.result.value as? [String: Any] else{
                completionHandler(nil, response.result.error?.localizedDescription)
                return
            }

            Utilities.printLog(key: "", value: jsonResponse)
            completionHandler(jsonResponse, response.result.error?.localizedDescription)

            manager.session.invalidateAndCancel()
        }
        
        /*
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
        ]
        
        let postData = NSMutableData(data: "username=zicom1".data(using: String.Encoding.utf8)!)
        postData.append("&password=aaaaaa".data(using: String.Encoding.utf8)!)
        postData.append("&latitude=12121212".data(using: String.Encoding.utf8)!)
        postData.append("&longitude=1212121".data(using: String.Encoding.utf8)!)
        postData.append("&type=enterprise".data(using: String.Encoding.utf8)!)
        postData.append("&device=\"mobile\": \"12121\"".data(using: String.Encoding.utf8)!)

        let request = NSMutableURLRequest(url: NSURL(string: "http://13.232.35.230/api/user/login")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if (error != nil)
            {
                Utilities.printLog(key: "Error", value: error ?? "No Error")
            }
            else
            {
                let httpResponse = response as? HTTPURLResponse
                let newStr = String(data: data!, encoding: String.Encoding.utf8)
                
                if newStr == nil
                {
                    Utilities.printLog(key: "Return String", value: "Empty return string")
                    completionHandler(nil, Constants.validationMessages.somethingwrong)
                }
                else
                {
                    if (httpResponse?.statusCode)! > 199 && (httpResponse?.statusCode)! < 299
                    {
                        Utilities.printLog(key: "", value: newStr!)
                        completionHandler(newStr, nil)
                    }
                }
            }
        })
        
        dataTask.resume()       */
    }
}
