//
//  DataModelDelegate.swift
//  TLB_ShaileshDodia
//
//  Created by Shailesh Dodia on 31/12/18.
//  Copyright © 2018 Shailesh Dodia. All rights reserved.
//

import Foundation

protocol DataModelDelegate
{
    func successResponse(response: Any?)

    func failResponse(response: Any?)
}
