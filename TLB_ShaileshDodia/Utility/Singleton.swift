//
//  Singleton.swift
//  TLB_ShaileshDodia
//
//  Created by Shailesh Dodia on 01/01/19.
//  Copyright © 2019 Shailesh Dodia. All rights reserved.
//

import Foundation

class Singleton: NSObject
{
    static let sharedManager = Singleton()

    var strToken: String?

    override init()
    {
        super.init()
    }
}
