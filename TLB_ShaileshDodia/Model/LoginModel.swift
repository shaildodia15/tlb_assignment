//
//  LoginModel.swift
//  TLB_ShaileshDodia
//
//  Created by Shailesh Dodia on 31/12/18.
//  Copyright © 2018 Shailesh Dodia. All rights reserved.
//

import Foundation
import ObjectMapper

class LoginModel
{
    static var delegateLoginModel: DataModelDelegate?
    
    static func requestToLogin(loginRequest: LoginRequest)
    {
        let requestDetails = loginRequest.toJSON()
        
        Utilities.printLog(key: "Request Parameters", value: requestDetails)
        
        WebService.requestServiceWith(url: Constants.urls.urlLogin, parameters: requestDetails) { (response, error) in
            
            if response != nil
            {
                if let keyResponse : LoginResponse = Mapper<LoginResponse>().map(JSONObject:response)
                {
                    if keyResponse.success == true
                    {
                        delegateLoginModel?.successResponse(response: keyResponse)
                    }
                    else
                    {
                        delegateLoginModel?.failResponse(response: keyResponse.message)
                    }
                }
                else
                {
                    delegateLoginModel?.failResponse(response:Constants.validationMessages.somethingwrong)
                }
            }
            else
            {
                if let message = error
                {
                    delegateLoginModel?.failResponse(response:message)
                }
                else
                {
                    delegateLoginModel?.failResponse(response:Constants.validationMessages.somethingwrong)
                }
            }
        }
    }
}


class LoginRequest: Mappable
{
    var username : String?
    var password : String?
    var type : String?
    var latitude : String?
    var longitude : String?
    var device : String? 

    init() {
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map)
    {
        username <- map["username"]
        password <- map["password"]
        `type` <- map["type"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
        device <- map["device"]
    }
}

class LoginResponse: Mappable
{
    var success : Bool?
    var message : String?
    var data = loginData()
    
    init() {
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map)
    {
        success <- map["success"]
        message <- map["message"]
        data <- map["data"]
    }
}

class loginData: Mappable
{
    var id : String?
    var token : String?
    var name : String?
    var asset : String?

    init() {
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map)
    {
        id <- map["id"]
        token <- map["token"]
        name <- map["name"]
        asset <- map["asset"]
    }
}
