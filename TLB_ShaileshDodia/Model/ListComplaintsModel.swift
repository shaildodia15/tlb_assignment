//
//  ListComplaintsModel.swift
//  TLB_ShaileshDodia
//
//  Created by Shailesh Dodia on 01/01/19.
//  Copyright © 2019 Shailesh Dodia. All rights reserved.
//

import Foundation
import ObjectMapper

class ListComplaintsModel
{
    static var delegateListComplaints: DataModelDelegate?
    
    static func requestToListComplaints(listComplaintsRequest: ListComplaintsRequest)
    {
        let requestDetails = listComplaintsRequest.toJSON()
        
        Utilities.printLog(key: "Request Parameters", value: requestDetails)
        
        WebService.requestServiceWith(url: Constants.urls.urlListComplaint, parameters: requestDetails) { (response, error) in
            
            if response != nil
            {
                if let keyResponse : ListComplaintResponse = Mapper<ListComplaintResponse>().map(JSONObject:response)
                {
                    if keyResponse.success == true
                    {
                        delegateListComplaints?.successResponse(response: keyResponse)
                    }
                    else
                    {
                        delegateListComplaints?.failResponse(response: keyResponse.message)
                    }
                }
                else
                {
                    delegateListComplaints?.failResponse(response:Constants.validationMessages.somethingwrong)
                }
            }
            else
            {
                if let message = error
                {
                    delegateListComplaints?.failResponse(response:message)
                }
                else
                {
                    delegateListComplaints?.failResponse(response:Constants.validationMessages.somethingwrong)
                }
            }
        }
    }
}


class ListComplaintsRequest: Mappable
{
    var asset : String?
    var ticket : String?
    var status : String?    //Open, Cancelled, Closed
    var offset : Int?
    
    init() {
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map)
    {
        asset <- map["asset"]
        ticket <- map["ticket"]
        status <- map["status"]
        offset <- map["offset"]
    }
}

class ListComplaintResponse: Mappable
{
    var success : Bool?
    var message: String?
    var data = ComplaintData()
    
    init() {
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map)
    {
        success <- map["success"]
        data <- map["data"]
        message <- map["message"]
    }
}

class ComplaintData: Mappable
{
    var complaints = [ComplaintObj]()
    
    init() {
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map)
    {
        complaints <- map["complaints"]
    }
}

class ComplaintObj: Mappable
{
    var tid : String?
    var date_time : String?
    var status : String?
    var request_title : String?
    var request_description : String?
    var engineer_name : String?
    var engineer_contact_no : String?
    var resolution_title : String?
    var resolution_description : String?
    var complainant_name : String?
    var complainant_number : String?
    var estimated_date_visit : String?
    
    init() {
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map)
    {
        tid <- map["tid"]
        date_time <- map["date_time"]
        status <- map["status"]
        request_title <- map["request_title"]
        request_description <- map["request_description"]
        engineer_name <- map["engineer_name"]
        engineer_contact_no <- map["engineer_contact_no"]
        resolution_title <- map["resolution_title"]
        resolution_description <- map["resolution_description"]
        complainant_name <- map["complainant_name"]
        complainant_number <- map["complainant_number"]
        estimated_date_visit <- map["estimated_date_visit"]
    }
}
