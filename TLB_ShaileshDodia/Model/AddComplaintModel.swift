//
//  AddComplaintModel.swift
//  TLB_ShaileshDodia
//
//  Created by Shailesh Dodia on 01/01/19.
//  Copyright © 2019 Shailesh Dodia. All rights reserved.
//

import Foundation
import ObjectMapper

class AddComplaintModel
{
    static var delegateAddComplaintModel: DataModelDelegate?
    
    static func requestToAddComplaint(addComplaintRequest: AddComplaintRequest)
    {
        let requestDetails = addComplaintRequest.toJSON()
        
        Utilities.printLog(key: "Request Parameters", value: requestDetails)
        
        WebService.requestServiceWith(url: Constants.urls.urlRaiseComplain, parameters: requestDetails) { (response, error) in
            
            if response != nil
            {
                if let keyResponse : AddComplaintResponse = Mapper<AddComplaintResponse>().map(JSONObject:response)
                {
                    if keyResponse.success == true
                    {
                        delegateAddComplaintModel?.successResponse(response: keyResponse)
                    }
                    else
                    {
                        delegateAddComplaintModel?.failResponse(response: keyResponse.message)
                    }
                }
                else
                {
                    delegateAddComplaintModel?.failResponse(response:Constants.validationMessages.somethingwrong)
                }
            }
            else
            {
                if let message = error
                {
                    delegateAddComplaintModel?.failResponse(response:message)
                }
                else
                {
                    delegateAddComplaintModel?.failResponse(response:Constants.validationMessages.somethingwrong)
                }
            }
        }
    }
}


class AddComplaintRequest: Mappable
{
    var registered_name : String?
    var registered_number : String?
    var complainant_name : String?
    var complainant_number : String?
    var asset : String?
    var title : String?
    var description : String?
    var latitude : String?
    var longitude : String?
    
    init() {
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map)
    {
        registered_name <- map["registered_name"]
        registered_number <- map["registered_number"]
        complainant_name <- map["complainant_name"]
        complainant_number <- map["complainant_number"]
        asset <- map["asset"]
        title <- map["title"]
        description <- map["description"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
}

class AddComplaintResponse: Mappable
{
    var success : Bool?
    var message : String?
    
    init() {
    }
    required init?(map: Map) {
    }
    
    func mapping(map: Map)
    {
        success <- map["success"]
        message <- map["message"]
    }
}
